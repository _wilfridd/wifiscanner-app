/*
 * Copyright (C) 2015 - Michael Zanetti <michael.zanetti@ubuntu.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "iwlist.h"
//#include <QDebug>
#include <QNetworkInterface>
#include <QDBusInterface>
#include <QDBusArgument>
#include <iostream>
using namespace std;

Iwlist::Iwlist(QObject *parent) :
    QAbstractListModel(parent),
    m_process(0),
    m_scanning(false)
{
    m_frequencyChannels24GHz.insert(2412, 1);
    m_frequencyChannels24GHz.insert(2417, 2);
    m_frequencyChannels24GHz.insert(2422, 3);
    m_frequencyChannels24GHz.insert(2427, 4);
    m_frequencyChannels24GHz.insert(2432, 5);
    m_frequencyChannels24GHz.insert(2437, 6);
    m_frequencyChannels24GHz.insert(2442, 7);
    m_frequencyChannels24GHz.insert(2447, 8);
    m_frequencyChannels24GHz.insert(2452, 9);
    m_frequencyChannels24GHz.insert(2457, 10);
    m_frequencyChannels24GHz.insert(2462, 11);
    m_frequencyChannels24GHz.insert(2467, 12);
    m_frequencyChannels24GHz.insert(2472, 13);
    m_frequencyChannels24GHz.insert(2484, 14);



    m_frequencyChannels5GHz.insert(5035, 7);
    m_frequencyChannels5GHz.insert(5040, 8);
    m_frequencyChannels5GHz.insert(5045, 9);
    m_frequencyChannels5GHz.insert(5055, 11);
    m_frequencyChannels5GHz.insert(5060, 12);
    m_frequencyChannels5GHz.insert(5080, 16);
    m_frequencyChannels5GHz.insert(5170, 34);
    m_frequencyChannels5GHz.insert(5180, 36);
    m_frequencyChannels5GHz.insert(5190, 38);
    m_frequencyChannels5GHz.insert(5200, 40);
    m_frequencyChannels5GHz.insert(5210, 42);
    m_frequencyChannels5GHz.insert(5220, 44);
    m_frequencyChannels5GHz.insert(5230, 46);
    m_frequencyChannels5GHz.insert(5240, 48);
    m_frequencyChannels5GHz.insert(5250, 50);
    m_frequencyChannels5GHz.insert(5260, 52);
    m_frequencyChannels5GHz.insert(5270, 54);
    m_frequencyChannels5GHz.insert(5280, 56);
    m_frequencyChannels5GHz.insert(5290, 58);
    m_frequencyChannels5GHz.insert(5300, 60);
    m_frequencyChannels5GHz.insert(5310, 62);
    m_frequencyChannels5GHz.insert(5320, 64);
    m_frequencyChannels5GHz.insert(5500, 100);
    m_frequencyChannels5GHz.insert(5510, 102);
    m_frequencyChannels5GHz.insert(5520, 104);
    m_frequencyChannels5GHz.insert(5530, 106);
    m_frequencyChannels5GHz.insert(5540, 108);
    m_frequencyChannels5GHz.insert(5550, 110);
    m_frequencyChannels5GHz.insert(5560, 112);
    m_frequencyChannels5GHz.insert(5570, 114);
    m_frequencyChannels5GHz.insert(5580, 116);
    m_frequencyChannels5GHz.insert(5590, 118);
    m_frequencyChannels5GHz.insert(5600, 120);
    m_frequencyChannels5GHz.insert(5610, 122);
    m_frequencyChannels5GHz.insert(5620, 124);
    m_frequencyChannels5GHz.insert(5630, 126);
    m_frequencyChannels5GHz.insert(5640, 128);
    m_frequencyChannels5GHz.insert(5660, 132);
    m_frequencyChannels5GHz.insert(5670, 134);
    m_frequencyChannels5GHz.insert(5680, 136);
    m_frequencyChannels5GHz.insert(5690, 138);
    m_frequencyChannels5GHz.insert(5700, 140);
    m_frequencyChannels5GHz.insert(5710, 142);
    m_frequencyChannels5GHz.insert(5720, 144);
    m_frequencyChannels5GHz.insert(5745, 149);
    m_frequencyChannels5GHz.insert(5755, 151);
    m_frequencyChannels5GHz.insert(5765, 153);
    m_frequencyChannels5GHz.insert(5775, 155);
    m_frequencyChannels5GHz.insert(5785, 157);
    m_frequencyChannels5GHz.insert(5795, 159);
    m_frequencyChannels5GHz.insert(5805, 161);
    m_frequencyChannels5GHz.insert(5825, 165);
    m_frequencyChannels5GHz.insert(4915, 183);
    m_frequencyChannels5GHz.insert(4920, 184);
    m_frequencyChannels5GHz.insert(4925, 185);
    m_frequencyChannels5GHz.insert(4935, 187);
    m_frequencyChannels5GHz.insert(4940, 188);
    m_frequencyChannels5GHz.insert(4945, 189);
    m_frequencyChannels5GHz.insert(4960, 192);
    m_frequencyChannels5GHz.insert(4980, 196);
}

bool Iwlist::running() const
{
    return m_process != 0 || m_scanning;
}

int Iwlist::count()
{
    return rowCount(QModelIndex());
}

QString Iwlist::error() const
{
    return m_error;
}

int Iwlist::rowCount(const QModelIndex &parent) const
{
    return m_list.count();
}

QVariant Iwlist::data(const QModelIndex &index, int role) const
{
    return QVariant();
}

WiFi *Iwlist::get(int index) const
{
    if (index >= 0 && index < m_list.count()) {
        return m_list.at(index);
    }
    return 0;
}

void Iwlist::start()
{
    if (running()) {
        return;
    }

    QString wifiDevice = findWiFiDevice();
    qDebug() << "found wifi device: " << wifiDevice;
    requestScan(wifiDevice);
}

void Iwlist::readAll()
{
    QString path = findWiFiDevice();

    QDBusInterface deviceIface("org.freedesktop.NetworkManager",
                               path,
                               "org.freedesktop.NetworkManager.Device.Wireless",
                               QDBusConnection::systemBus());

    QDBusMessage reply = deviceIface.call("GetAllAccessPoints");
    if (reply.type() == QDBusMessage::ErrorMessage) {
        qDebug() << "Error info:" << reply.errorMessage();
        qDebug() << "On:" << deviceIface.service() << deviceIface.path() << deviceIface.interface();
        return;
    }
    QDBusArgument aps = qvariant_cast<QDBusArgument> ( reply.arguments().first());

    QList<QDBusObjectPath> apPaths;
    aps.beginArray();
    while (!aps.atEnd())
    {
        QDBusObjectPath path;
        aps >> path;
        apPaths.append(path);
        qDebug() << "hve ap path;" << path.path();
    }
    aps.endArray();

    QStringList seenList;

    foreach (const QDBusObjectPath &apPath, apPaths) {
        QDBusInterface apIface("org.freedesktop.NetworkManager",
                               apPath.path(),
                               "org.freedesktop.NetworkManager.AccessPoint",
                               QDBusConnection::systemBus());
        QByteArray hwAddr = apIface.property("HwAddress").toByteArray();
        seenList.append(hwAddr);
        int idx = findWiFi(hwAddr);
        WiFi *currentWifi;
        if (idx < 0) {
            currentWifi = new WiFi(this);
        } else {
            currentWifi = m_list.at(idx);
        }
        currentWifi->setName(apIface.property("Ssid").toByteArray());
        currentWifi->setHwAddr(hwAddr);
        currentWifi->setFrequency(apIface.property("Frequency").toReal() / 1000);
        if (currentWifi->frequency() > 5) {
            currentWifi->setBand(Iwlist::WiFiBand5GHz);
            currentWifi->setChannel(m_frequencyChannels5GHz.value(apIface.property("Frequency").toInt()));
        } else {
            currentWifi->setBand(Iwlist::WiFiBand24GHz);
            currentWifi->setChannel(m_frequencyChannels24GHz.value(apIface.property("Frequency").toInt()));
        }
        currentWifi->setLevel(apIface.property("Strength").toInt() / 2 - 100);
        qDebug() << "apIf" << currentWifi->level();
        if (idx < 0) {
            beginInsertRows(QModelIndex(), m_list.count(), m_list.count());
            m_list.append(currentWifi);
            endInsertRows();
            emit countChanged();
        } else {
            emit dataChanged(index(idx), index(idx));
        }
    }

    for (int i = 0, removed = 0; i < m_list.count(); i++) {
        if (!seenList.contains(m_list.at(i - removed)->hwAddr())) {

            beginRemoveRows(QModelIndex(), i - removed, i - removed);
            m_list.takeAt(i - removed)->deleteLater();
            endRemoveRows();
            emit countChanged();
            removed++;
        }
    }
}

QString Iwlist::findWiFiDevice()
{
    QDBusInterface iface("org.freedesktop.NetworkManager",
                         "/org/freedesktop/NetworkManager",
                         "org.freedesktop.NetworkManager",
                         QDBusConnection::systemBus());
    QDBusMessage reply = iface.call("GetDevices");

    if (reply.type() == QDBusMessage::ErrorMessage) {
        qDebug() << "Error calling GetAllDevices:" << reply.errorMessage();
        return QString();
    }

    QDBusArgument dbusArgs = qvariant_cast<QDBusArgument> ( reply.arguments().first());

    QList<QDBusObjectPath> paths;
    dbusArgs.beginArray();
    while (!dbusArgs.atEnd())
    {
        QDBusObjectPath path;
        dbusArgs >> path;
        paths.append(path);
    }
    dbusArgs.endArray();

    foreach (const QDBusObjectPath &path, paths) {
        QDBusInterface deviceInterface("org.freedesktop.NetworkManager",
                                       path.path(),
                                       "org.freedesktop.NetworkManager.Device",
                                       QDBusConnection::systemBus());
        uint deviceType = deviceInterface.property("DeviceType").toUInt();
        if (deviceType == 2) {
            return path.path();
        }
    }
    return QString();
}

void Iwlist::requestScan(const QString &device)
{
    QDBusInterface deviceIface ("org.freedesktop.NetworkManager", device, "org.freedesktop.NetworkManager.Device.Wireless", QDBusConnection::systemBus());
    QDBusMessage reply = deviceIface.call("RequestScan", QVariantMap());
    cout << "scan requested " << reply.errorMessage().toStdString() << " " << device.toStdString() <<endl;
    if (!reply.errorMessage().isEmpty()) {
        m_error = reply.errorMessage();
        emit errorChanged();
        scanFinished();
    } else {
        m_error.clear();
        emit errorChanged();
        m_scanning = true;
        emit runningChanged();

        QDBusConnection::systemBus().connect("org.freedesktop.NetworkManager", device, "org.freedesktop.NetworkManager.Device.Wireless", "ScanDone", this, SLOT(scanFinished()));
    }

}

void Iwlist::scanFinished()
{
    qDebug() << "Scan finished";
    m_scanning = false;
    readAll();
    emit runningChanged();
}

int Iwlist::findWiFi(const QString &hwAddr)
{
    for (int i = 0; i < m_list.count(); i++) {
        if (m_list.at(i)->hwAddr() == hwAddr) {
            return i;
        }
    }
    return -1;
}


int WiFi::color() const
{
    int total = 0;
    for (int i = 0; i < m_hwAddr.length(); i++) {
        bool ok;
        int number = m_hwAddr.left(i+1).right(1).toInt(&ok);
        if (ok) {
            total += number;
        }
    }
    return total;
}
