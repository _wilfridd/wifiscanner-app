/*
 * Copyright (C) 2015 - Michael Zanetti <michael.zanetti@ubuntu.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IWLIST_H
#define IWLIST_H

#include <QObject>
#include <QProcess>
#include <QAbstractListModel>
#include <QSortFilterProxyModel>

class WiFi;

class Iwlist : public QAbstractListModel
{
    Q_OBJECT
    Q_ENUMS(WiFiBand)
    Q_PROPERTY(bool running READ running NOTIFY runningChanged)
    Q_PROPERTY(int count READ count NOTIFY countChanged)
    Q_PROPERTY(QString error READ error NOTIFY errorChanged)
public:
    enum WiFiBand {
        WiFiBand24GHz,
        WiFiBand5GHz
    };

    explicit Iwlist(QObject *parent = 0);

    bool running() const;
    Q_INVOKABLE int count();
    QString error() const;


    int rowCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;

    Q_INVOKABLE WiFi* get(int index) const;
    Q_INVOKABLE int findWiFi(const QString &hwAddr);

signals:
    void runningChanged();
    void countChanged();
    void errorChanged();

public slots:
    void start();

private slots:
    QString findWiFiDevice();
    void readAll();
    void requestScan(const QString &device);

    void scanFinished();
private:

    QProcess *m_process;
    bool m_scanning;
    QString m_error;

    QList<WiFi*> m_list;

    QHash<int, int> m_frequencyChannels24GHz;
    QHash<int, int> m_frequencyChannels5GHz;

};

class WiFi: public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString name READ name CONSTANT)
    Q_PROPERTY(QString hwAddr READ hwAddr CONSTANT)
    Q_PROPERTY(int channel READ channel CONSTANT)
    Q_PROPERTY(int level READ level CONSTANT)
    Q_PROPERTY(int color READ color CONSTANT)
    Q_PROPERTY(Iwlist::WiFiBand band READ band CONSTANT)
    Q_PROPERTY(qreal frequency READ frequency CONSTANT)

public:
    WiFi(QObject *parent = 0): QObject(parent), m_band(Iwlist::WiFiBand24GHz) {}

    QString name() const {return m_name;}
    void setName(const QString &name) { m_name = name; }

    QString hwAddr() const { return m_hwAddr; }
    void setHwAddr(const QString &hwAddr) { m_hwAddr = hwAddr; }

    int  channel() const {return m_channel;}
    void setChannel(int channel) { m_channel = channel; }

    int  level() const {return m_level;}
    void setLevel(int level) { m_level = level; }

    int color() const;

    Iwlist::WiFiBand band() const { return m_band; }
    void setBand(Iwlist::WiFiBand band) { m_band = band; }

    qreal frequency() const { return m_frequency; }
    void setFrequency(qreal frequency) { m_frequency = frequency; }

private:
    QString m_name;
    QString m_hwAddr;
    int m_channel;
    int m_level;
    Iwlist::WiFiBand m_band;
    qreal m_frequency;
};
#include <QDebug>
class WiFiFilterModel: public QSortFilterProxyModel
{
    Q_OBJECT
    Q_PROPERTY(Iwlist* sourceModel READ sourceModel WRITE setSourceModel NOTIFY sourceModelChanged)
    Q_PROPERTY(Iwlist::WiFiBand band READ band WRITE setBand NOTIFY bandChanged)
    Q_PROPERTY(int count READ count NOTIFY countChanged)
public:
    WiFiFilterModel(QObject *parent = 0): QSortFilterProxyModel(parent), m_sourceModel(0), m_band(Iwlist::WiFiBand24GHz) {}

    int count() const { return rowCount(); }
    Iwlist* sourceModel() const { return m_sourceModel; }
    void setSourceModel(Iwlist* sourceModel) { m_sourceModel = sourceModel; QSortFilterProxyModel::setSourceModel(sourceModel); emit sourceModelChanged(); }

    Iwlist::WiFiBand band() const { return m_band; }
    void setBand(Iwlist::WiFiBand band) { m_band = band; emit bandChanged(); }

    bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const {
        if (!sourceModel()) {
            return false;
        }
//        qDebug() << "filtering" << m_sourceModel->get(source_row)->name() << m_band << m_sourceModel->get(source_row)->band();
//        qDebug() << "accepting:" << (m_sourceModel->get(source_row)->band() == m_band);
        return m_sourceModel->get(source_row)->band() == m_band;
    }

    Q_INVOKABLE WiFi* get(int idx) const {
        return m_sourceModel->get(mapToSource(this->index(idx, 0)).row());
    }
signals:
    void bandChanged();
    void sourceModelChanged();
    void countChanged();

private:
    Iwlist* m_sourceModel;
    Iwlist::WiFiBand m_band;
};
#endif // IWLIST_H
